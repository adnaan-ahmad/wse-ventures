import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, FlatList, TouchableOpacity, Image } from 'react-native'
import { MaterialIcons, SimpleLineIcons } from '@expo/vector-icons'

export default function Connect() {

  const [users, setUsers] = useState([])

  useEffect(() => {
    fetch('https://rickandmortyapi.com/api/character')
    .then(response => response.json())
    .then(data => setUsers(data.results))
  }, [])

  return (
    <View style={styles.container}>
        <View style={styles.icons}>
            <MaterialIcons name="keyboard-backspace" size={24} color="#91B3A2" />
            <MaterialIcons name="search" size={24} color="#91B3A2" />
        </View>
        <Text style={styles.heading}>You May Know</Text>
        <Text style={styles.subHeading}>Get to know Incredible people!</Text>
        <View style={styles.border}></View>
        {users.length !== 0 ? 
        <FlatList 
            numColumns={2}
            keyExtractor={(item) => item.id}
            data={users}
            renderItem={({item}) => (
            <View style={styles.item}>
                <View style={styles.options}><SimpleLineIcons name="options-vertical" size={12} color="#D4DDE0" /></View>
                <Image source={{uri : item.image}} style={styles.image} />
                <Text style={styles.name}>{item.name.split(' ').slice(0,2).join(' ')}</Text>
                <Text style={styles.followers}>1468 followers</Text>
                <TouchableOpacity>
                    <Text style={styles.button}>CONNECT</Text>
                </TouchableOpacity>
            </View>
            )}
        />
        : <Text style={styles.loading}>Loading...</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    paddingHorizontal: 20,
    backgroundColor: '#1D525E',
    width : '100%',
    paddingVertical: '2%',
  },
  options:{
    alignSelf:'flex-end',
    marginRight:'2%',
  },
  loading:{
    color:'#CFE7C7',
    textAlign:'center',
    fontSize: 18,
    marginTop: '60%',
  },
  border:{
    borderWidth: .34,
    borderColor: 'black',
    width:'113%',
    marginLeft:'-6%',
    marginTop: '6%',
  },
  icons:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  heading:{
    color:'#CFE7C7',
    textAlign:'center',
    fontSize: 18,
    marginTop: '3%',
    fontWeight:'bold',
  },
  subHeading:{
    color:'#BFC52E',
    textAlign:'center',
    fontSize: 12,
  },
  item: {
    flex: 1,
    marginHorizontal: 10,
    marginTop: 24,
    backgroundColor: '#FFFFFF',
    width: 140,
    height: 120,
    borderRadius:10,
    alignItems:'center',
    justifyContent : 'center',
    textAlign:'center',
  },
  image: {
      height:30,
      width:30,
      borderRadius:20,
      marginTop: '-7.5%',
  },
  name:{
    fontWeight:'bold',
    color: '#00394D',
  },
  button : {
    marginTop: '3%',
    fontWeight:'bold',
    paddingTop: 6,
    backgroundColor: '#BFC52E',
    fontSize: 11,
    height:29,
    width:72,
    borderRadius:4,
    textAlign:'center',
    color: '#FFFFFF',
    opacity: 0.9
  },
  followers:{
    color: '#698F8A',
    fontSize: 10.5,
  },
});
