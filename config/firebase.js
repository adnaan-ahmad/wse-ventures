// import * as firebase from 'firebase/app';
// import 'firebase/auth';

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyC0NPVr-6bO3JexqXS097Z7B1AyVNIlztI",
    authDomain: "react-firebase-428d0.firebaseapp.com",
    projectId: "react-firebase-428d0",
    storageBucket: "react-firebase-428d0.appspot.com",
    messagingSenderId: "689265605846",
    appId: "1:689265605846:web:2c6a49816710acb9a21d77"
};

let Firebase;

// if (firebase.apps.length === 0) {
  Firebase = firebase.initializeApp(firebaseConfig);
// }

export default Firebase;