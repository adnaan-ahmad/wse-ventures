import * as React from 'react';
import * as WebBrowser from 'expo-web-browser';
import { ResponseType } from 'expo-auth-session';
import * as Google from 'expo-auth-session/providers/google';
import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider, signInWithCredential } from 'firebase/auth';
import { View, Button, Alert } from 'react-native';

// import Firebase from './config/firebase'

import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyC0NPVr-6bO3JexqXS097Z7B1AyVNIlztI",
    authDomain: "react-firebase-428d0.firebaseapp.com",
    projectId: "react-firebase-428d0",
    storageBucket: "react-firebase-428d0.appspot.com",
    messagingSenderId: "689265605846",
    appId: "1:689265605846:web:2c6a49816710acb9a21d77"
};

let Firebase;

// if (firebase.apps.length === 0) {
  Firebase = firebase.initializeApp(firebaseConfig);
// }

const auth = Firebase.auth()

// Initialize Firebase
// initializeApp({
//   /* Config */
// });

WebBrowser.maybeCompleteAuthSession();

export default function App() {

  const [request, response, promptAsync] = Google.useIdTokenAuthRequest(
    {
      clientId: '689265605846-5aukk3gj7qoi4csec8gka5pf7t42ivuk.apps.googleusercontent.com',
      // 689265605846-5aukk3gj7qoi4csec8gka5pf7t42ivuk.apps.googleusercontent.com
      },
  );

  React.useEffect(() => {
    if (response?.type === 'success') {
      // console.log('Logged in!', `Hi ${(response.params)}`);
      const { id_token } = response.params;
      
      // const auth = getAuth();
      // const provider = new GoogleAuthProvider();
      // const credential = provider.credential(id_token);
      const credential = GoogleAuthProvider.credential(id_token);
      const credentialPromise = signInWithCredential(auth, credential);
      credentialPromise.then((credential) => {
        console.log(credential);
      })

      // Alert.alert('Logged in!', `Hi ${(response.url)}`);
      // console.log('Logged in!', `Hi....... ${(response.params)}`);

    }
  }, [response]);

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <Button
      disabled={!request}
      title="Login"
      onPress={() => {
        promptAsync();
        }}
    />
    </View>
  );
}























// import React from 'react';
// import { StyleSheet, View, Text, Alert } from 'react-native';
// // import * as firebase from 'firebase/app'
// // import Firebase from './config/firebase'
// import Expo from 'expo'
// import * as Facebook from 'expo-facebook';
// import * as GoogleAuthentication from 'expo-google-app-auth';
// import firebase from 'firebase';

// const auth = Firebase.auth()

// // const firebaseConfig = {
// //   apiKey: "AIzaSyC0NPVr-6bO3JexqXS097Z7B1AyVNIlztI",
// //   authDomain: "react-firebase-428d0.firebaseapp.com",
// //   projectId: "react-firebase-428d0",
// //   storageBucket: "react-firebase-428d0.appspot.com",
// //   messagingSenderId: "689265605846",
// //   appId: "1:689265605846:web:2c6a49816710acb9a21d77",
// //   // measurementId: "G-4MQYBDQZ4S"
// // }

// // Firebase.initializeApp(firebaseConfig)

// export default class App extends React.Component {

//   // componentDidMount() {

//   //   auth.onAuthStateChanged((user) => {
//   //     if (user != null) {
//   //       console.log(user)
//   //     }
//   //   })
//   // }

//   // async loginWithFacebook() {

//   //   //ENTER YOUR APP ID 
//   //   const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('1835738646611707', { permissions: ['public_profile'] })

//   //   if (type == 'success') {

//   //     const credential = Firebase.auth.FacebookAuthProvider.credential(token)

//   //     auth.signInWithCredential(credential).catch((error) => {
//   //       console.log(error)
//   //     })
//   //   }
//   // }

//   async loginWithFacebook() {
//     try {
//       await Facebook.initializeAsync({
//         appId: '1835738646611707',
//       });


//       const { type, token, expirationDate, permissions, declinedPermissions } =
//         await Facebook.logInWithReadPermissionsAsync({
//           permissions: ['public_profile', 'email'],
//           // behavior: 'web'
//         });
//       alert("Hii");
//       if (type === 'success') {
//         // Get the user's name using Facebook's Graph API
//         const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
//         Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
//       } else {
//         // type === 'cancel'
//       }
//     } catch ({ message }) {
//       alert(`Facebook Login Error: ${message}`);
//       console.log(message)
//     }
//   }

//   loginWithGoogle = () =>
//     GoogleAuthentication.logInAsync({
//       androidStandaloneAppClientId: '689265605846-5aukk3gj7qoi4csec8gka5pf7t42ivuk.apps.googleusercontent.com',
//       // iosStandaloneAppClientId: 'IOS_STANDALONE_APP_CLIENT_ID',
//       scopes: ['profile', 'email']
//     })
//       .then((logInResult) => {
//         if (logInResult.type === 'success') {
//           const { idToken, accessToken } = logInResult;
//           const credential = firebase.auth.GoogleAuthProvider.credential(
//             idToken,
//             accessToken
//           );

//           return firebase.auth().signInWithCredential(credential);
//           // Successful sign in is handled by firebase.auth().onAuthStateChanged
//         }
//         return Promise.reject(); // Or handle user cancelation separatedly
//       })
//       .catch((error) => {
//         // ...
//       });

//   render() {
//     return (
//       <View style={styles.container}>
//         <Text
//           // onPress={() => auth.createUserWithEmailAndPassword('ahmad20.adnan95@gmail.com', 'Kohefiza1@')}
//           // onPress={() => auth.signInWithEmailAndPassword('ahmad20.adnan95@gmail.com', 'Kohefiza1@').then(user => console.log(user))}
//           // onPress={() => this.loginWithFacebook()}
//           onPress={() => this.loginWithGoogle()}
//           style={styles.heading}>Login</Text>
//       </View>
//     )
//   }

// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
